const path = require('path')
const fs = require('fs')
const express = require('express')
const Stream = require('stream')
const events = require('events')
const expressWebSocket = require('express-ws')
const { OPEN } = require('ws')
const { isFinite } = require('underscore')
const ffmpeg = require('fluent-ffmpeg');
var ffmpegPath = "./ffmpeg/bin/ffmpeg.exe";

ffmpeg.setFfmpegPath(ffmpegPath);

const event = new events.EventEmitter()

// 转码指令
const uris = [
  "rtsp://admin:1234qwer@192.168.31.101:554/cam/realmonitor?channel=2&subtype=0",
  "rtsp://admin:1234qwer@192.168.31.101:554/cam/realmonitor?channel=3&subtype=0",
  "rtsp://ceshi:1234qwer@192.168.31.131:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_1"
]
const server = express()

expressWebSocket(server, null, {
  perMessageDeflate: true
});

const ffmpegs = []
function getFfmpeg(index) {
  if (!ffmpegs[index]) {
    ffmpegs[index] = ffmpeg(uris[index])
      .inputOptions(
        '-flags', 'low_delay',
        '-fflags', 'nobuffer',
        '-buffer_size', '655360',
        '-analyzeduration', '7000000',
        '-max_delay', '1',
        '-rtsp_transport', 'tcp'
      )
      .outputOptions(
        '-vcodec',
        'libx264',
        '-preset',
        'ultrafast',
        '-rtsp_transport', 'tcp',
        '-threads',
        '1',
        '-r',
        '5',
        '-an',
        '-max_delay', '1',
        '-flush_packets', '1',
        '-tune', 'zerolatency',
      ).outputFormat("flv").videoCodec("copy").noAudio().seek(3)
      .fpsOutput(10)
  }
  return ffmpegs[index].clone()
}


uris.forEach((_uri, id) => {

  const stream = new Stream.Writable()
  let initChunk = null
  const sendFlvHead = []
  event.on('ws-close', ws => {
    const index = sendFlvHead.indexOf(ws)
    if (index === -1) return
    sendFlvHead.splice(ws, 1)
  })
  stream._write = function (chunk, en, cb) {
    if (!initChunk) initChunk = chunk
    if (websocketPools[id]) {
      websocketPools[id].forEach(ws => {
        if (ws.readyState !== OPEN) return
        if (!sendFlvHead.includes(ws)) {
          sendFlvHead.push(ws)
          ws.send(initChunk, { binary: true })
        }
        ws.send(chunk, { binary: true })
      })
    }

    cb()
  }

  getFfmpeg(id).pipe(stream)

})

const websocketPools = []

server.ws("/rtsp/:id", function (ws, req) {


  let id = isFinite(req.params.id || '0') ? parseInt(`${req.params.id}`, 10) : 0
  if (id < 0) id = 0
  else if (id > uris.length - 1) id = uris.length - 1

  if (!websocketPools[id]) websocketPools[id] = []
  websocketPools[id].push(ws)

  ws.on('close', () => {
    event.emit('ws-close', ws)
    const index = websocketPools[id].indexOf(ws)
    if (index === -1) return
    websocketPools[id].splice(index, 1)
  })

  console.log(`id:${id}`, uris[id])


})
server.listen(8888);
console.log("express listened")